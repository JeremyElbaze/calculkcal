class CalculKcal{
    constructor(){
        this.gender = document.querySelectorAll('.form-step1-genre');
        this.age = document.querySelector('#form-step1-age');
        this.size = document.querySelector('#form-step1-size');
        this.weight = document.querySelector('#form-step1-weight');
        this.activity = document.querySelectorAll('.multiplicateur');
        this.isMen;
    }

    getGenre(){
        this.gender.forEach(sexe => {
            if(sexe.checked){
                if(sexe.value === "men"){
                    this.isMen = true;
                }else if(sexe.value === "woman"){
                    this.isMen = false;
                }
            }
        });
        return this.isMen;
    }

    getAge(){
        if(this.age != ""){
            return parseInt(this.age.value, 10);
        }
    }

    getSize(){
        if(this.size != ""){
            return parseInt(this.size.value, 10);
        }
    }

    getWeight(){
        if(this.weight != ""){
            return parseInt(this.weight.value, 10);
        }
    }

    getActivity(){
        let toto = 1.0;

        this.activity.forEach(el => {
            if(el.checked){
                toto = el.value;
            }
        });

        return toto;
    }

    calcul(){
        if(this.getGenre()){
            let formule = (66.5 + (13.75 * this.getWeight()) + (5 * this.getSize()) - (6.77 * this.getAge())) * this.getActivity();
            return formule;
        }else{
            let formule = (655.1 + (9.56 * this.getWeight()) + (1.85 * this.getSize()) - (4.67 * this.getAge())) * this.getActivity();
            return formule;
        }
    }
}


const unCalcul = new CalculKcal();
let btn = document.querySelector('.form-send');
let displayResult = document.querySelector('.popin-result');


//let rs = {};

// btn.addEventListener('click', (e) => {
//     e.preventDefault();
//     rs.genre = unCalcul.getGenre();
//     rs.age = unCalcul.getAge();
//     rs.size = unCalcul.getSize();
//     rs.weight = unCalcul.getWeight();
//     rs.activity = unCalcul.getActivity()
//     console.log(`Genre : ${rs.genre}, Age : ${rs.age}, Taille : ${rs.size}, Poids : ${rs.weight}, Activité : ${rs.activity}`);
// });


btn.addEventListener('click', (e) => {
    e.preventDefault();
    const unCalcul = new CalculKcal();
    displayResult.innerHTML = `${parseInt(unCalcul.calcul(), 10)} kcal`;
    displayResult.style.padding = "10px";
});















